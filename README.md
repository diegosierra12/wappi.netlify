Autor: Diego Alexander Sierra Sepúlveda.
Descripción: Proyecto de automatización para realizar pruebas sobre el sitio: https://wappi.netlify.com/login, se realizaron pruebas 
sobre las siguientes funcionalidades:

1. LogIn.
2. LogOut.
3. Offers.
4. Save Coupons.
5. Sort Products.
6. Update Profile

Herramientas utilizadas: IDE: Intellij IDEA, Selenium, ChromeDriver versión 78.0.3904.70, Maven, TestNG

Notas generales:

1. Se debe verificar la versión de Chrome, en caso de no tener la versión 78, se debe actualizar a esta versión para poder realizar la ejecución
de todos los test.
2. Se realiza la implementación de un origen de datos (DataDriven) para que funcione desde un archivo de Excel, este se encuentra en la ruta 
del proyecto: "src/test/resources/data/DataDriven.xlsx". Esta implementación simula la hoja de excel como una tabla de base de datos, es decir que para 
realizar cualquier consulta sobre ella se debe realizar por medio de un query.
3. Dentro de todos los test se solicita como parámetro un id (strId), este id hace referencia al campo ID del archivo DataDriven.xlsx, es decir,
que dentro de la prueba va a utilizar el registro relacionado a ese id.
4. Luego de realizar la ejecución de cada uno de los test, la automatización genera un archivo html con el reporte del test ejecutado en la carpeta:
"ExtentReports", el nombre del archivo de la siguiente manera: "ReportResultsyyyyMMdd HHmmss.html"
5. Para los test de Update Profile se crea la carpeta images en la ruta: \src\test\resources\images, en esta carpeta se pueden subir las imágenes que se desean
relacionar al perfil dentro de cada prueba, el nombre del archivo debe ir en el DataDriven en la hoja: "Users" en el campo: "NameImageProfile". Esto en caso
de querer cambiar la imagen a subir en el perfil.
6. Para la funcionalidad de reclamar el cupón de bienvenida se implementa que este se guarde en el DataDriven en la hoja: "Users" en el campo: "WelcomeCouponCode"
7. Los reportes de cada uno de los test fueron implementados con ExtentReports.